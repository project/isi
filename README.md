# INTRODUCTION
------------

Important Site Information module provides functionality of
block and tray, both contain urgent or very important information
such as user agreement or licenses.

# REQUIREMENTS
------------

This module requires the following modules:

 * Paragraphs library

# INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.

# CONFIGURATION
-------------

1. Go to the Structure / Block Layout / Add custom block
2. Select ISI as a block type
3. Add content sections as a new paragraphs or choose them from paragraphs library (paragraph should be preliminarily created or promoted into paragraphs library)
4. Do not forget set the section visibility ("Enable" checkbox)
5. Place the block to the layout (Structure / Block Layout)

## Theming
=============
The main block could be themed by template block--isi.html.twig
The paragraphs section could be themed by template paragraph--isi-section.html.twig

The module has the next additional classes on a wrapper:
- `.inline-isi-wrapper` - for inline block;
- `.isi-tray` - for tray block

# REFERENCES
-----------

# MAINTAINERS
-----------

Current maintainers:
 * Ievgen Kyvgyla <hello@kivgila.pro> - https://kivgila.pro

This project has been sponsored by:
 * EPAM Systems <https://www.epam.com/>
