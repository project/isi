<?php

namespace Drupal\isi\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Allows to add ISI settings.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraph_behaviors_paragraph_isi_settings",
 *   label = @Translation("Paragraph ISI settings"),
 *   description = @Translation("Allows to add ISI settings."),
 *   weight = 0,
 * )
 */
class ParagraphISISettingsBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    if ($paragraphs_type->id() === 'isi_section') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['custom_isi_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom id'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_isi_id'),
    ];

    $form['custom_isi_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_isi_class'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $custom_isi_id = $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_isi_id');
    $custom_isi_class = $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_isi_class');
    $summary = [];
    $summary[] = $this->t('Custom id: @element', ['@element' => $custom_isi_id]);
    $summary[] = $this->t('Custom class: @element', ['@element' => $custom_isi_class]);

    return $summary;
  }

}
