(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.ISIDrawer = {
    attach: function (context) {

      if(context !== document) {
        return;
      }

      // isi tray behaviour.
      function initTray() {
        var $isiContainer = $('.jsIsiContainer', context);
        var $isiBtnMinimize = $('.jsIsiMinimize', context);
        var $isiSwapText = $('.js-swap-text', context);
        initTrayScroll();
        $isiBtnMinimize.once('isiBtnMinimizeClick').on(
          'click',
          function () {
            $isiContainer.toggleClass('opened isi-drawer-expanded isi-drawer-collapsed');
            $isiSwapText.toggleClass('hide');
          }
        );

      }

      function initScrollUpdate() {
        var $window = $(window);
        var $isiSection;
        if(window.location.href.indexOf('importantsafetyinformation') > 0) {
          $isiSection = $('.info-with-summary-section', context);
        }
        else{
          $isiSection = $('.isi-section', context);
        }
        var $isiTray = $('.isi-tray', context);
        var windowOffset = $window.scrollTop();
        var docViewBottom = windowOffset + $window.height() - $isiTray.height();
        var vertTopMargin = 80; // conceal tray when 80 px above isiTop anchor
        if($isiSection.length && $isiTray.length) {
          var elemTop = $isiSection.offset().top - vertTopMargin;
          if(elemTop <= docViewBottom) {
            $($isiTray).addClass('hide-tray');
          }
          else{
            $($isiTray).removeClass('hide-tray');
          }
        }
      }

      function initTrayScroll() {
        $('.isi-scrollable', context).once('initTrayScroll').scrollbar({
          autoScrollSize: false,
          scrolly: $('.external-scroll-y', context)
        });
      }

      // Minimize tray on scroll event
      $(window).once('initScrollUpdate').on(
        'scroll resize',
        function () {
          initScrollUpdate();
        }
      );

      initTray();
      initScrollUpdate();

    }
  };

})(jQuery, Drupal);
