(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.ISIInitialVisit = {
    attach: function (context) {

      if(context !== document) {
        return;
      }

      // test and set cookie as needed
      var cookieName = "InitialSiteVisitCookie";
      var initVisit = $.cookie(cookieName);

      if(initVisit === true || initVisit === null || typeof initVisit === "undefined") {
        // this is the initial visit, cookie being initialized and temporarily
        // set to true until animation occurs;
        $.cookie(cookieName, true, {path: '/'});
        var $isi = $("#isi-top", context);
        var position = $(window).scrollTop();
        var vertOffset = 20; // top padding to leave above ISI when scrolling
        var $isiSwapText = $('.js-swap-text', context);
        // into focus
        if(position <= 1) {
          setTimeout(
            function () {
              if(drupalSettings.isi.stickyfooter) {
                $('html, body').animate({scrollTop: $isi.offset().top - vertOffset}, 500);
              }
              else{
                $('.jsIsiContainer', context).addClass('isi-drawer-expanded');
                $isiSwapText.toggleClass('hide');
              }
              $.cookie(cookieName, false, {expires: 365, path: '/'});
            }, 1000
          );
        }
      }
      else{
        // this is not the initial visit, cookie exists- keep it false
        $.cookie(cookieName, false, {path: '/'});
      }

    }
  };

})(jQuery, Drupal, drupalSettings);
