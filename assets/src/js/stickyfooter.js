(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.ISISticky = {
    attach: function (context) {

      if(context !== document) {
        return;
      }

      function initScrollUpdate() {
        var $window = $(window);
        var $isiSection;
        if(window.location.href.indexOf('importantsafetyinformation') > 0) {
          $isiSection = $('.info-with-summary-section', context);
        }
        else{
          $isiSection = $('.isi-section', context);
        }
        var $isiTray = $('.isi-tray', context);
        var windowOffset = $window.scrollTop();
        var docViewBottom = windowOffset + $window.height() - $isiTray.height();
        if($isiSection.length && $isiTray.length) {
          var elemTop = $isiSection.offset().top;
          if(elemTop <= docViewBottom) {
            $($isiTray).addClass('hide-tray');
          }
          else{
            $($isiTray).removeClass('hide-tray');
          }
        }
      }

      // check whether to hide/show sticky footer on scroll event
      $(window).once('initScrollUpdate').scroll($.debounce(100, function (e) {
        initScrollUpdate();
      }));

      function isiClickHandler() {
        $('#isi-drawer-wrapper', context).once('isi-drawer-wrapper-click').on(
          'click',
          function () {
            $('#isi-drawer-handle', context).toggleClass('open-drawer close-drawer');
            $('#isi-drawer-wrapper', context).toggleClass('isi-drawer-collapsed isi-drawer-expanded');
          }
        );
      }

      // check whether to hide/show sticky footer on scroll event
      $(window).scroll($.debounce(100, function (e) {
        initScrollUpdate();
      }));

      var vertOffset = 20; // top padding to leave above ISI when scrolling
      $('.isi-jump-link', context).once('isi-drawer-wrapper-click').on(
        'click',
        function () {
          $('html, body').animate({scrollTop: $("#isi-top", context).offset().top - vertOffset}, 500);
        }
      );

      isiClickHandler();
      initScrollUpdate();
    }
  };

})(jQuery, Drupal);
