"use strict";

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.ISIInitialVisit = {
    attach: function attach(context) {
      if (context !== document) {
        return;
      }

      var cookieName = "InitialSiteVisitCookie";
      var initVisit = $.cookie(cookieName);

      if (initVisit === true || initVisit === null || typeof initVisit === "undefined") {
        $.cookie(cookieName, true, {
          path: '/'
        });
        var $isi = $("#isi-top", context);
        var position = $(window).scrollTop();
        var vertOffset = 20;
        var $isiSwapText = $('.js-swap-text', context);

        if (position <= 1) {
          setTimeout(function () {
            if (drupalSettings.isi.stickyfooter) {
              $('html, body').animate({
                scrollTop: $isi.offset().top - vertOffset
              }, 500);
            } else {
              $('.jsIsiContainer', context).addClass('isi-drawer-expanded');
              $isiSwapText.toggleClass('hide');
            }

            $.cookie(cookieName, false, {
              expires: 365,
              path: '/'
            });
          }, 1000);
        }
      } else {
        $.cookie(cookieName, false, {
          path: '/'
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
